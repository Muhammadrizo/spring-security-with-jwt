package uz.rizo.demo.models;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
public class AuthenticationResponse {

    private String jwt;

    public String getJwt() {
        return jwt;
    }
}
