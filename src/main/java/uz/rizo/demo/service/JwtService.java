package uz.rizo.demo.service;

import org.springframework.stereotype.Component;
import uz.rizo.demo.models.AuthenticationRequest;
import uz.rizo.demo.models.AuthenticationResponse;

@Component
public interface JwtService {
    AuthenticationResponse createAuthenticationToken(AuthenticationRequest authenticationRequest) throws Exception;
}
