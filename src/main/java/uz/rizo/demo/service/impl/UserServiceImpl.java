package uz.rizo.demo.service.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.rizo.demo.common.Role;
import uz.rizo.demo.entity.User;
import uz.rizo.demo.repository.UserRepository;
import uz.rizo.demo.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User addNewUser(User user) {
        user.setRole (Role.USER);
        user.setPassword (passwordEncoder.encode (user.getPassword ()));
        return userRepository.save (user);
    }

    public List<User> showUser() {
        return userRepository.findAll ();
    }
}
