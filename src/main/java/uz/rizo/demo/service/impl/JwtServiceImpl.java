package uz.rizo.demo.service.impl;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import uz.rizo.demo.models.AuthenticationRequest;
import uz.rizo.demo.models.AuthenticationResponse;
import uz.rizo.demo.service.JwtService;
import uz.rizo.demo.util.JwtUtil;

@Service
public class JwtServiceImpl implements JwtService {
    private final AuthenticationManager authenticationManager;
    private final MyUserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;

    public JwtServiceImpl(AuthenticationManager authenticationManager, MyUserDetailsService userDetailsService, JwtUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public AuthenticationResponse createAuthenticationToken(AuthenticationRequest authenticationRequest) throws Exception {
        try{
            authenticationManager.authenticate (new UsernamePasswordAuthenticationToken (authenticationRequest.getUsername (),authenticationRequest.getPassword ()));
        }catch (BadCredentialsException e){
            throw new Exception ("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername (authenticationRequest.getUsername ());

        final String jwt = jwtUtil.generateToken (userDetails);

        return new AuthenticationResponse (jwt);
    }
}
