package uz.rizo.demo.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.rizo.demo.common.Role;
import uz.rizo.demo.entity.User;
import uz.rizo.demo.repository.UserRepository;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User addNewUser(User user);
    List<User> showUser();
}
