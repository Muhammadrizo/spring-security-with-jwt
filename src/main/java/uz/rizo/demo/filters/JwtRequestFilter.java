package uz.rizo.demo.filters;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.rizo.demo.service.impl.MyUserDetailsService;
import uz.rizo.demo.util.JwtUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    private final MyUserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;

    public JwtRequestFilter(MyUserDetailsService userDetailsService, JwtUtil jwt) {
        this.userDetailsService = userDetailsService;
        this.jwtUtil = jwt;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String authorizationHeader = httpServletRequest.getHeader ("X-AUTH-TOKEN");
        String username = null;
        String jwt = null;
        if (authorizationHeader != null && authorizationHeader.startsWith ("Bearer ")) {
            jwt = authorizationHeader.substring (7);
            username = jwtUtil.extractUsername (jwt);
        }

        if (username != null && SecurityContextHolder.getContext ().getAuthentication () == null) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername (username);
            if (jwtUtil.validateToken (jwt, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken (
                        userDetails, null, userDetails.getAuthorities ());
                usernamePasswordAuthenticationToken
                        .setDetails (new WebAuthenticationDetailsSource ().buildDetails (httpServletRequest));
                SecurityContextHolder.getContext ().setAuthentication (usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter (httpServletRequest, httpServletResponse);
    }
}
