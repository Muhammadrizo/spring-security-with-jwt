package uz.rizo.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import uz.rizo.demo.entity.User;
import uz.rizo.demo.models.AuthenticationRequest;
import uz.rizo.demo.models.AuthenticationResponse;
import uz.rizo.demo.service.JwtService;
import uz.rizo.demo.service.UserService;

import java.util.List;

@RestController
@RequestMapping
public class UserController {


    private final UserService userService;
    private final JwtService jwtService;

    public UserController(UserService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody User user) {
        return ResponseEntity.ok (userService.addNewUser (user));
    }

    @GetMapping("/get")
    public ResponseEntity<List<User>> addUser() {
        return ResponseEntity.ok (userService.showUser ());
    }

    @PostMapping("/authentication")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        return ResponseEntity.ok (jwtService.createAuthenticationToken (authenticationRequest));
    }
}
